//
//  main.m
//  DropDown program
//
//  Created by Click Labs133 on 10/5/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
