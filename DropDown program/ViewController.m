//
//  ViewController.m
//  DropDown program
//
//  Created by Click Labs133 on 10/5/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *arrName;

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UITableView *tableData;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@end

@implementation ViewController
@synthesize submitButton;
@synthesize tableData;
@synthesize deleteBtn;
UIButton *btn;
int i;
UITableViewCell *cell;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    arrName=[[NSMutableArray alloc]init];
    [submitButton addTarget:self action:@selector(submitButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [deleteBtn addTarget:self action:@selector(deleteRow:) forControlEvents:UIControlEventTouchUpInside];
    tableData.hidden=YES;
    deleteBtn.hidden=YES;
    arrName=@[@"vijay",@"tamanna",@"pooja",@"suchi",@"shiv",@"koshal",@"akshda",@"damini",@"garima",@"ujjwal",@"shreya",@"rahil"];
    
}
-(void)submitButtonPressed:(UIButton *)sender
{
    tableData.hidden=NO;
    deleteBtn.hidden=NO;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrName.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell1"];
    
   
   
   //button properties
   btn=[[UIButton alloc]init];
    btn.frame=CGRectMake(0,05,270,40);
    btn.backgroundColor=[UIColor grayColor];
    [cell.contentView addSubview:btn];
    cell.backgroundColor=[UIColor blackColor];
    for (i=0; i<12; i++) {
         [btn setTitle:arrName[indexPath.row] forState:UIControlStateNormal];
        if (i==0) {
            [btn addTarget:self action:@selector(name1:) forControlEvents:UIControlEventTouchUpInside];
            }
      else  if (i==1) {
            [btn addTarget:self action:@selector(name2:) forControlEvents:UIControlEventTouchUpInside];
          }
     else   if (i==2) {
         [btn addTarget:self action:@selector(name3:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        else if (i==3)
        {
            [btn addTarget:self action:@selector(name4:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if(i==4)
    {
        [btn addTarget:self action:@selector(name5:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if(i==5)
    {
       [btn addTarget:self action:@selector(name6:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if(i==6)
    {
        [btn addTarget:self action:@selector(name7:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if(i==7)
    {
        [btn addTarget:self action:@selector(name8:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if(i==8)
    {
        [btn addTarget:self action:@selector(name9:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if(i==9)
    {
        [btn addTarget:self action:@selector(name10:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if(i==10)
    {
        [btn addTarget:self action:@selector(name11:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if(i==11);
    {
        [btn addTarget:self action:@selector(name12:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    }
    return cell;
    }

-(void)name1:(UIButton *)sender
{
   
        [submitButton setTitle:@"vijay" forState:UIControlStateNormal];
        submitButton.backgroundColor=[UIColor blueColor];
        submitButton.titleLabel.textColor=[UIColor whiteColor];
    }

-(void)name2:(UIButton *)sender
{
    [submitButton setTitle:@"tamanna" forState:UIControlStateNormal];
    submitButton.backgroundColor =[UIColor grayColor];
    submitButton.titleLabel.textColor=[UIColor blueColor];
}
-(void)name3:(UIButton *)sender
{
    [submitButton setTitle:@"pooja" forState:UIControlStateNormal];
    submitButton.backgroundColor =[UIColor brownColor];
    submitButton.titleLabel.textColor=[UIColor greenColor];
}
-(void)name4:(UIButton *)sender
{
    [submitButton setTitle:@"suchi" forState:UIControlStateNormal];
    submitButton.titleLabel.textColor=[UIColor yellowColor];
}
-(void)name5:(UIButton *)sender
{
    [submitButton setTitle:@"shiv" forState:UIControlStateNormal];
    submitButton.titleLabel.textColor=[UIColor grayColor];
}
-(void)name6:(UIButton *)sender
{
    [submitButton setTitle:@"koshal" forState:UIControlStateNormal];
    submitButton.titleLabel.textColor=[UIColor brownColor];
}
-(void)name7:(UIButton *)sender
{
    [submitButton setTitle:@"akshda" forState:UIControlStateNormal];
    submitButton.titleLabel.textColor=[UIColor purpleColor];
}
-(void)name8:(UIButton *)sender
{
    [submitButton setTitle:@"damini" forState:UIControlStateNormal];
    submitButton.titleLabel.textColor=[UIColor darkGrayColor];
}
-(void)name9:(UIButton *)sender
{
    [submitButton setTitle:@"garima" forState:UIControlStateNormal];
    submitButton.titleLabel.textColor=[UIColor cyanColor];
}
-(void)name10:(UIButton *)sender
{
    [submitButton setTitle:@"ujjwal" forState:UIControlStateNormal];
    submitButton.titleLabel.textColor=[UIColor magentaColor];
}
-(void)name11:(UIButton *)sender
{
    [submitButton setTitle:@"shreya" forState:UIControlStateNormal];
    submitButton.titleLabel.textColor=[UIColor orangeColor];
}
-(void)name12:(UIButton *)sender
{
    [submitButton setTitle:@"rahil" forState:UIControlStateNormal];
    submitButton.titleLabel.textColor=[UIColor yellowColor];
}






-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if(editingStyle==UITableViewCellEditingStyleDelete)
        
    {
        
        [arrName removeObjectAtIndex:indexPath.row];
        
        [tableData deleteRowsAtIndexPaths:[NSMutableArray arrayWithObjects:indexPath, nil ] withRowAnimation:UITableViewRowAnimationFade];
        
    }
    
}
-(void)deleteRow:(UIButton *)sender
{
    [tableData setEditing:YES animated:YES];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
